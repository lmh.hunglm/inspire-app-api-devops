terraform {
  backend "s3" {
    bucket         = "inspire-app-api-devops-tfstate"
    key            = "inspire-app.tfstate"
    region         = "ap-southeast-1"
    encrypt        = true
    dynamodb_table = "inspire-app-api-devops-tfstate-lock"
  }
}

provider "aws" {
  region  = "ap-southeast-1"
  version = "~> 2.50.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}
