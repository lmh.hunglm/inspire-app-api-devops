variable "prefix" {
  default = "iaad"
}

variable "project" {
  default = "inspire-app-api-devops"
}

variable "contact" {
  default = "lmh.hunglm@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

